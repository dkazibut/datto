var Scraper = {
	getCharacterFrequency: function getCharacterFrequencyF(str) {
		// Create an object that has the letters and the amount of time they appear in a string
		// e.g., the string 'foo' generates { 'f': 1, 'o': 2 }
		var freqObj = {};
		
		for (var i = 0; i < str.length; i++) {
			var letter = str.charAt(i);
			if (freqObj[letter]) {
				freqObj[letter]++;
			} else {
				freqObj[letter] = 1;
			}
		}

		return freqObj;
	},
	getWord: function getWordF(path) {
		$.get(path).done(function(data){
			var stringToCheck = data;
			
			data = data.replace(/[\.,-\/#!$?%\^&\*;:{}=\-_`~()]/g,"").toLowerCase()
			
			var words = data.split(' ').sort(),
			uniqueWordList = words.filter(function(el, pos){
				return words.indexOf(el) == pos;
			});
			
			// Build an object that has the word plus all the letter counts
			// e.g., { 'word': 'foo', 'letters': { 'f': 1, 'o': 2 } }
			
			// This is probably overkill for this demo, but I wanted to write
			// it as though I'm at work and inevitably someone will come along and
			// say, "wouldn't it be nice if...", so I'm creating an object that 
			// will serve me well down the road
			var wordsArray = uniqueWordList.map(function(word){
				var wordObj = {},
					tmpObj = Scraper.getCharacterFrequency(word);
	
				wordObj['word'] = word;
				wordObj['letters'] = Scraper.getCharacterFrequency(word);
	
				// I want to get the largest number for recurring letters
				var tmpArray = [];
				for (var key in tmpObj) {
					tmpArray.push(tmpObj[key]);
				}
				wordObj['largest'] = Math.max.apply(Math, tmpArray);
	
				return wordObj;
			});
			
			// Find the word with the most recurring characters
			var largestNumber = 0,
				wordWithMostRecurringLetters = '';

			for (var i = 0; i < wordsArray.length; i++) {
				if ( wordsArray[i].largest > largestNumber ) {
					largestNumber = wordsArray[i].largest;
					wordWithMostRecurringLetters = wordsArray[i].word;
				}
			}
			
			// Print the string we're checking and the winning word to the screen
			$('#string-to-check').html(stringToCheck);
			$('#winning-word').text(wordWithMostRecurringLetters);
			
		})
		.fail(function(){
			alert('There was an error getting the file');
		});
	}
};

$(document).ready(function(){
	Scraper.getWord('http://www.kazibut.org/datto/data/romeo.txt');
});
